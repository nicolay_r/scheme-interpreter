module Main where
import System.Environment
import Text.ParserCombinators.Parsec
import Data.Map
import Data.List
import Data.Maybe
data Token = AtomString String | 
	     	 List [Token]
	    deriving (Show)

data Expr =   Id String  		-- built-in функции
			| UId String 		-- определенные пользователем
			| Atom NodeType 	-- атомарное значение
			| Var String 	    -- переменные. Они определяются после 
								-- потому что через map мы подставим значение, переданное 
								-- через эту перемеднную 
			| Expr [Expr]		-- выражение - это список выражений
			deriving (Show)
data NodeType =	  INT Int 		-- целый тип
				| DOUBLE Double
				| BOOL Bool     
				| STRING String -- как обычная строка, так и переменная
				| NULL			-- отсутствие карирования
								-- по умолчанию так инициализируются переменные функциии
				deriving (Show)
-- словарь новых функций
type VarTable = Map String NodeType
--	переменные -  сама функция
type Attributes = (Expr, Expr)
type Vocabulary = [(String, Attributes)]

data Runtime = Runtime 
				{
					v :: Vocabulary, -- словарь пользовательских функций
					expr :: Expr,
					vt :: VarTable 	-- стек текущих переменных переменные
				}

whiteSpace :: String
whiteSpace = "\v\r\n\t\f "

pLexem :: Parser String
pLexem =  many $ noneOf "\v\r\n\t\f() "

separators :: Parser ()
separators = skipMany $ oneOf whiteSpace

pList :: Token -> Parser Token
pList (List l) = 
	  	-- in ""
	   do	try (char '\"')
	    	x <- pLexem
	    	char '\"'
	    	separators
	    	pList (List (l ++ [AtomString ("\"" ++ x ++ "\"") ]))
	    -- in Scope 
		<|> do  
		  	try (char '(')
		  	inScope <- pList (List [])
		  	pList $ List $ l ++ [inScope] 
		-- end of Scope
		<|> do 
			try (char ')')
			return ( List l )
		-- string
		<|> do 		    
			lookAhead (noneOf whiteSpace)
			lexem <- pLexem
			separators
			pList ( List ( l ++ [AtomString lexem])) 
		-- spaces
		<|> do 
			lookAhead (oneOf whiteSpace)
			separators
			pList (List l)
		-- end of file	
		<|> return ( List l )
-- определение типов 

isBool :: String -> Bool
isBool "True" = True
isBool "False" = True
isBool x = False

isInt :: String -> Bool
isInt [] = True
isInt (h:t) = if (h >= '0' && h <= '9') 
					then isInt t
					else False
isString :: String -> Bool
isString s = if ((s!!0) == '\"' && (last s) == '\"') 
				then True
				else False
-- построение выражения по дереву
findFunction :: String -> Vocabulary -> Maybe Attributes
findFunction s [] = Nothing
findFunction s (h:t) = if (s == (fst h)) then Just (snd h)
										else findFunction s t
defineId :: String -> Expr 
defineId  name = if isJust (find (\p -> p == name) builtInFunctions) 
					then  Id name
					else  UId name 
-- здесь добавим, что первый арг в скобках - Id или UId
createExpr :: Vocabulary-> Token -> Expr
createExpr v (List ((AtomString name):t)) = (Expr ([defineId name] ++ [(createExpr v x) | x <- t]))
createExpr v (AtomString x) 	
			    | isBool x 	 = 	Atom $ BOOL ((read x) :: Bool)
				| isInt x 	 =	Atom $ INT ((read x) :: Int)
				| isString x =  Atom $ STRING x
 				| otherwise  = 	Var x

builtInFunctions :: [String]
builtInFunctions = ["define", "+", "-", "*", "if", ">", "<", "=", "modulo"]

-- задача функций - положить оставить на стеке один элемент типа Node Type
-- так же передается Хеш таблица с переменными
add ::NodeType -> NodeType -> NodeType
add  (INT x) (INT y) = INT (x+y)
add  x	y = error "Incompatible types. Expected - Int."

sub ::NodeType -> NodeType -> NodeType
sub  (INT x) (INT y) = INT (x-y)
sub  x	y = error "Incompatible types. Expected - Int."

mul ::NodeType -> NodeType -> NodeType
mul  (INT x) (INT y) = INT (x*y)
mul  x	y = error "Incompatible types. Expected - Int."

isTrue :: NodeType -> Bool
isTrue (BOOL True) = True
isTrue (BOOL False) = False
isTrue x = error $ "Incompatible type. Expected - Bool."

bOp :: (Int -> Int -> Bool) -> [NodeType] -> NodeType
bOp f [(INT x),(INT y)] = if  (f x y) then BOOL True
									  else BOOL False
bOp f ((INT x):(INT y):t) = if (f x y) 
								then bOp f ((INT y):t)
								else BOOL False

binOp :: (Int -> Int -> Int) -> [NodeType] -> NodeType
binOp f ((INT x):(INT y):t) = binOp f ((INT (f x y)):t)
binOp f [INT x] = INT x
binOp f list = error $ "Too enought arguments. Expected 2..n."
takeName :: Expr -> String 
takeName (Expr ((UId name):t)) = name
takeName x = error $ "undefined pattern. Expr = " ++ show (x)

defineUf :: Runtime -> (NodeType, Vocabulary)
defineUf  (Runtime {expr = (Expr t), vt = vt, v = v}) = 
		(NULL, v ++ [(takeName signature,(signature,code))])
			where 
				signature = t!!0
				code = t!!1

builtIn :: String -> Runtime -> NodeType
builtIn "if" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= if (isTrue(fst(eval (Runtime {expr = condition, vt = vt, v = v})))) 
					then fst $ eval (Runtime {expr = then_case, vt = vt, v = v})
					else fst $ eval (Runtime {expr = else_case, vt = vt, v = v})
			where 
				condition = t!!0
				then_case = t!!1
				else_case = t!!2
builtIn "+"	(Runtime {expr = (Expr t), vt = vt, v = v}) 
		= Data.List.foldl add  (INT 0) [ fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t ]
builtIn "-"	(Runtime {expr = (Expr t), vt = vt, v = v}) 
		= Data.List.foldl sub  (fst (eval(Runtime {expr = (t!!0), vt = vt, v = v}))) 
				[ fst (eval(Runtime {expr = (t!!i), vt = vt, v = v})) | i <- [1..(length t - 1)] ]
builtIn "*"	(Runtime {expr = (Expr t), vt = vt, v = v}) 
		=  Data.List.foldl mul  (INT 1) [ fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t ]	
builtIn ">" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= bOp (>) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t] 

builtIn "<" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= bOp (<) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t] 

builtIn "=" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= bOp (==) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t] 
builtIn "modulo" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= binOp (mod) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t]
-- пользовательские функции помещают код в рантайм, и запускают на имполнение
-- а так же заполняет хеш таблицу переменными
-- здесь надо добавить копию мапы, и в нее вносить изменения.
-- проблема в том, что мы перезаписываем сразу старые. Это неверно.
userFunction :: Attributes ->  Runtime -> VarTable -> NodeType
userFunction (Expr [], code) runtime new_vt = fst (eval (runtime {expr = code, vt = new_vt}))
userFunction (Expr ((Var var):t), code) (Runtime {expr = (Expr(val:e)), vt = vt, v = v}) new_vt = 
				userFunction (Expr t, code) (Runtime { v = v, expr = (Expr e), vt = vt }) (Data.Map.insert var new_val new_vt)
				where
					  new_runtime = Runtime {v = v, expr = val, vt = vt}
					  new_val = fst (eval new_runtime)
userFunction (Expr ((UId var):t), code) runtime new_vt = userFunction (Expr t, code) runtime new_vt
userFunction a runtime new_vt= error $ "Attributes : " ++ (show a)
-- вычисляет выражение и кладет результат вычисления на стек

eval :: Runtime -> (NodeType, Vocabulary)
eval (Runtime {v = v, expr =  Expr ((Id "define"):t), vt = vt}) =
						defineUf (Runtime {expr = (Expr t), vt = vt, v = v})
eval (Runtime {v = v, expr =  Expr ((Id name):t), vt = vt})  = 
						(builtIn name  (Runtime {expr = (Expr t), vt = vt, v = v}), v)
eval (Runtime {v = v, expr =  Expr ((UId name):t), vt = vt}) = 
						if (isJust function)
								then (userFunction (fromJust function) (Runtime {expr = (Expr t), vt = vt, v = v}) vt, v)
								else error $ "undefined function : " ++ name ++ "\n" ++ (show (Expr ((UId name):t)))
						where 
							function = findFunction name v
eval (Runtime {v = v, expr = (Atom x), vt = vt})  =  (x, v)
eval (Runtime {v = v, expr = (Var name), vt = vt})  = 
						if (isJust var) 
								then (fromJust var, v)
								else error $ "undefined variable : " ++ name ++ " vars " ++ (show vt)
						where var = (Data.Map.lookup name vt)
eval (Runtime {v = v, expr = Expr [Expr list], vt = vt}) = 
		eval (Runtime {v = v, expr = Expr list , vt = vt})

eval runtime = error $ "handle error. Expr = " ++ (show (expr runtime)) ++ "\n Vocabulary = " ++ (show (v runtime))

-- запуск интерпертатора
run :: Token -> Runtime -> IO ()
run (List []) runtime = putStr $ "done."
run (List (h:t)) runtime= do
						putStr $ show (expr) ++ "\n"
						putStr $ "> "++ show (fst calc) ++ "\n"
						run (List t) (runtime {v = snd calc}) 
					where 
							expr = createExpr (v runtime) h
							calc = eval (runtime {expr = (createExpr (v runtime) h)})
start :: Runtime -> Parser Token -> String -> IO ()
start  runtime parser str = do 
						tree <- treeBuilder parser str 
						run tree runtime
treeBuilder :: Parser Token -> String -> IO Token
treeBuilder p input = case (parse p "" input) of 
					Left e -> do {
								   putStr "parse error at ";
								   print e;
								   return (List [])
								 }
					Right x -> return x
main :: IO ()
main = getArgs 
	>>= return.(!!0) 
	>>= readFile 
	>>= start (Runtime {v = [], expr = Atom NULL, vt = empty} ) (pList (List []))



