
module TokenBuilder where 


import Text.ParserCombinators.Parsec


data Token = AtomString String | 
	     	 List [Token]
	    deriving (Show)

treeBuilder :: Parser Token -> String -> IO Token
treeBuilder p input = case (parse p "" input) of 
					Left e -> do {
								   putStr "parse error at ";
								   print e;
								   return (List [])
								 }
					Right x -> return x

whiteSpace :: String
whiteSpace = "\v\r\n\t\f "

pLexem :: Parser String
pLexem =  many $ noneOf "\v\r\n\t\f() "

separators :: Parser ()
separators = skipMany $ oneOf whiteSpace

pList :: Token -> Parser Token
pList (List l) = 
	   do	try (char '\"')
	    	x <- pLexem
	    	char '\"'
	    	separators
	    	pList (List (l ++ [AtomString ("\"" ++ x ++ "\"") ]))
		<|> do -- In Scope
		  	try (char '(')
		  	inScope <- pList (List [])
		  	pList $ List $ l ++ [inScope] 
		<|> do -- End Of Scope
			try (char ')')
			return ( List l )
		<|> do -- String
			lookAhead (noneOf whiteSpace)
			lexem <- pLexem
			separators
			pList ( List ( l ++ [AtomString lexem])) 
		<|> do -- Spaces
			lookAhead (oneOf whiteSpace)
			separators
			pList (List l)
		<|> return ( List l )