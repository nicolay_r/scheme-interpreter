module Types where 

import Data.Map

data Expr =   Id String  			-- built-in функции
			| UId String 			-- определенные пользователем
			| Atom NodeType 		-- атомарное значение
			| Var String 	    	-- переменные
			| Expr [Expr]			-- выражение - это список выражений
			deriving (Show)
data NodeType =	  INT Int 			-- целый тип
				| DOUBLE Double 	-- вещественный тип
				| BOOL Bool     	
				| STRING String 	-- как обычная строка, так и переменная
				| NULL				-- по умолчанию так инициализируются переменные функциии

instance Show NodeType where
	show (INT x) = show x
	show (DOUBLE x) = show x
	show (BOOL x) = if (x) then "#t" else "#f"
	show (STRING x) = show x
	show (NULL) = show "()" 

type VarTable = Map String NodeType -- словарь новых функций
type Attributes = (Expr, Expr)  	-- атрибуты функции
type Vocabulary = [(String, Attributes)]

data Runtime = Runtime 
		{
			v :: Vocabulary, 		-- словарь пользовательских функций
			expr :: Expr,			-- текущее выражение
			vt :: VarTable 			-- стек текущих переменных переменные
		}
	