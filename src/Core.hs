module Core where

import Types
import Data.Maybe
import Data.List
import Data.Map

findFunction :: String -> Vocabulary -> Maybe Attributes
findFunction s [] = Nothing
findFunction s (h:t) = if (s == (fst h)) then Just (snd h)
										else findFunction s t

eval :: Runtime -> (NodeType, Vocabulary)
eval (Runtime {v = v, expr =  Expr ((Id "define"):t), vt = vt}) =
						defineUf (Runtime {expr = (Expr t), vt = vt, v = v})
eval (Runtime {v = v, expr =  Expr ((Id name):t), vt = vt})  = 
						(builtIn name  (Runtime {expr = (Expr t), vt = vt, v = v}), v)
eval (Runtime {v = v, expr =  Expr ((UId name):t), vt = vt}) = 
						if (isJust function)
								then (userFunction (fromJust function) (Runtime {expr = (Expr t), vt = vt, v = v}) vt, v)
								else error $ "undefined function : " ++ name ++ "\n" ++ (show (Expr ((UId name):t)))
						where 
							function = findFunction name v
eval (Runtime {v = v, expr = (Atom x), vt = vt})  =  (x, v)
eval (Runtime {v = v, expr = (Var name), vt = vt})  = 
						if (isJust var) 
								then (fromJust var, v)
								else error $ "undefined variable : " ++ name ++ " vars " ++ (show vt)
						where var = (Data.Map.lookup name vt)
eval (Runtime {v = v, expr = Expr [Expr list], vt = vt}) = 
		eval (Runtime {v = v, expr = Expr list , vt = vt})

eval runtime = error $ "handle error. Expr = " ++ (show (expr runtime)) ++ "\n Vocabulary = " ++ (show (v runtime))


userFunction :: Attributes ->  Runtime -> VarTable -> NodeType
userFunction (Expr [], code) runtime new_vt = fst (eval (runtime {expr = code, vt = new_vt}))
userFunction (Expr ((Var var):t), code) (Runtime {expr = (Expr(val:e)), vt = vt, v = v}) new_vt = 
				userFunction (Expr t, code) (Runtime { v = v, expr = (Expr e), vt = vt }) (Data.Map.insert var new_val new_vt)
				where
					  new_runtime = Runtime {v = v, expr = val, vt = vt}
					  new_val = fst (eval new_runtime)
userFunction (Expr ((UId var):t), code) runtime new_vt = userFunction (Expr t, code) runtime new_vt
userFunction a runtime new_vt= error $ "userFunction "
	
builtInFunctions = ["define", "+", "-", "*", "if", ">", "<", "=", "modulo"]

defineUf :: Runtime -> (NodeType, Vocabulary)
defineUf  (Runtime {expr = (Expr t), vt = vt, v = v}) = 
		(NULL, v ++ [(takeName signature,(signature,code))])
			where 
				signature = t!!0
				code = t!!1

builtIn :: String -> Runtime -> NodeType
builtIn "if" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= if (isTrue(fst(eval (Runtime {expr = condition, vt = vt, v = v})))) 
					then fst $ eval (Runtime {expr = then_case, vt = vt, v = v})
					else fst $ eval (Runtime {expr = else_case, vt = vt, v = v})
			where 
				condition = t!!0
				then_case = t!!1
				else_case = t!!2

builtIn "+"	(Runtime {expr = (Expr t), vt = vt, v = v}) 
		= Data.List.foldl add  (INT 0) [ fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t ]
builtIn "-"	(Runtime {expr = (Expr t), vt = vt, v = v}) 
		= Data.List.foldl sub  (fst (eval(Runtime {expr = (t!!0), vt = vt, v = v}))) 
				[ fst (eval(Runtime {expr = (t!!i), vt = vt, v = v})) | i <- [1..(length t - 1)] ]
builtIn "*"	(Runtime {expr = (Expr t), vt = vt, v = v}) 
		=  Data.List.foldl mul  (INT 1) [ fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t ]	
builtIn ">" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= bOp (>) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t] 

builtIn "<" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= bOp (<) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t] 

builtIn "=" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= bOp (==) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t] 
builtIn "modulo" (Runtime {expr = (Expr t), vt = vt, v = v}) 
		= binOp (mod) [fst (eval(Runtime {expr = x, vt = vt, v = v})) | x <- t]

add ::NodeType -> NodeType -> NodeType
add  (INT x) (INT y) = INT (x+y)
add  x	y = error "Incompatible types. Expected - Int."

sub ::NodeType -> NodeType -> NodeType
sub  (INT x) (INT y) = INT (x-y)
sub  x	y = error "Incompatible types. Expected - Int."

mul ::NodeType -> NodeType -> NodeType
mul  (INT x) (INT y) = INT (x*y)
mul  x	y = error "Incompatible types. Expected - Int."

isTrue :: NodeType -> Bool
isTrue (BOOL True) = True
isTrue (BOOL False) = False
isTrue x = error $ "Incompatible type. Expected - Bool."

bOp :: (Int -> Int -> Bool) -> [NodeType] -> NodeType
bOp f [(INT x),(INT y)] = if  (f x y) then BOOL True
									  else BOOL False
bOp f ((INT x):(INT y):t) = if (f x y) 
								then bOp f ((INT y):t)
								else BOOL False

binOp :: (Int -> Int -> Int) -> [NodeType] -> NodeType
binOp f ((INT x):(INT y):t) = binOp f ((INT (f x y)):t)
binOp f [INT x] = INT x
binOp f list = error $ "Too enought arguments. Expected 2..n."

takeName :: Expr -> String 
takeName (Expr ((UId name):t)) = name
takeName x = error $ "undefined pattern. Expr = " ++ show (x)
