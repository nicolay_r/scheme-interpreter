module Main where
import System.Environment

import Text.ParserCombinators.Parsec
-------------------------------------
import TokenBuilder
import Types
import Core
-------------------------------------
import Data.Map
import Data.List
import Data.Maybe


isBool :: String -> Bool
isBool "True" = True
isBool "False" = True
isBool x = False

isInt :: String -> Bool
isInt [] = True
isInt (h:t) = if (h >= '0' && h <= '9') 
					then isInt t
					else False

isString :: String -> Bool
isString s = if ((s!!0) == '\"' && (last s) == '\"') 
				then True
				else False

defineId :: String -> Expr 
defineId  name = if isJust (find (\p -> p == name) builtInFunctions) 
					then  Id name
					else  UId name 

createExpr :: Vocabulary-> Token -> Expr
createExpr v (List ((AtomString name):t)) = (Expr ([defineId name] ++ [(createExpr v x) | x <- t]))
createExpr v (AtomString x) 	
			    | isBool x 	 = 	Atom $ BOOL ((read x) :: Bool)
				| isInt x 	 =	Atom $ INT ((read x) :: Int)
				| isString x =  Atom $ STRING x
 				| otherwise  = 	Var x

run :: Token -> Runtime -> IO ()
run (List []) runtime = return ()
run (List (h:t)) runtime = putStr ( show (expr) ++ "\n" ) 
						>> putStr ( "> "++ show (fst calc)  ++ "\n" ) 
						>> run (List t) (runtime {v = snd calc}) 
				where 
					expr = createExpr (v runtime) h
					calc = eval $ runtime {expr = (createExpr (v runtime) h)}
-- запуск интерпертатора
start :: Runtime -> Parser Token -> String -> IO ()
start  runtime parser str = do 
						tree <- treeBuilder parser str 
						run tree runtime

main :: IO ()
main = getArgs 
	>>= return.(!!0) 
	>>= readFile 
	>>= start (Runtime {v = [], expr = Atom NULL, vt = empty} ) (pList (List []))



